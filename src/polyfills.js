/* ... */
"use strict";
/* enable these */
require("core-js/es6/symbol");
require("core-js/es6/object");
require("core-js/es6/function");
require("core-js/es6/parse-int");
require("core-js/es6/parse-float");
require("core-js/es6/number");
require("core-js/es6/math");
require("core-js/es6/string");
require("core-js/es6/date");
require("core-js/es6/array");
require("core-js/es6/regexp");
require("core-js/es6/map");
require("core-js/es6/weak-map");
require("core-js/es6/set");
/* ... */
/* and these: */
/**
 * Date, currency, decimal and percent pipes.
 * Needed for: All but Chrome, Firefox, Edge, IE11 and Safari 10
 */
require("intl"); // Run `npm install --save intl`.
/**
 * Need to import at least one locale-data with intl.
 */
require("intl/locale-data/jsonp/en");
//# sourceMappingURL=polyfills.js.map