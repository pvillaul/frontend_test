// tuberia para filtrar por texto:titulo tasks en el buscador de tareas

import { Pipe, PipeTransform } from '@angular/core';
import { ToDo } from './todo';

@Pipe({
  name: 'filter',
  pure: false
})

export class FilterPipe implements PipeTransform {
  transform(items: any[], filter: ToDo): any {
  	if (!items || !filter) {  
            return items;  
        }  
        return items.filter(item => item.title.indexOf(filter.title) !== -1);
   }
}
