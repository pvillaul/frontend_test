// tuberia para mostrar los tasks mas recientes primero
import { Pipe } from '@angular/core';

@Pipe({
  name: 'reverse',
  pure: false
})

export class ReversePipe {
  transform (values: any[]) {
    if (values) {
      return values.reverse();
    }
  }

}
