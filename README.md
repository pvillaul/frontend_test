# Prueba técnica frontend GOI

Hola,

Antes de nada, muchas gracias por contactar con nosotros y dedicar parte de tu tiempo a la realización de este pequeña prueba técnica.

Esta prueba consiste en el desarrollo de la parte frontend de una sencilla SPA (Single Page Application) de gestión de tareas.

Se debe crear un servicio para obtener las tareas del siguiente endpoint:

`GET https://jsonplaceholder.typicode.com/todos`

Se deben mostrar solo las tareas del usuario con id 1.

![Wireframe](https://puu.sh/yWLQy/530cabea29.png "Wireframe Todo")

## Funciones

* Mostrar el listado de tareas.
* Filtrar las tareas a través de una caja de texto.
* Marcar/Desmarcar tareas como realizadas.
* Eliminar tareas
* Añadir nuevas tareas.

Los cambios realizados sobre el listado de tareas, no es necesario que tengan persistencia en backend. Al actualizar la página, se resetearán todos los datos, mostrando de nuevo el contenido original obtenido a través del endpoint facilitado.

## Requisitos

* Angular 4 o 5.
* El diseño debe de ser responsive.
* La interface debe estar dividida en componentes, según tu criterio.
* Hoja de estilos con SASS.

## Bonus

* Test unitarios.
* Diseño atractivo.
* Comentarios en el código.

## Workflow

* Haz un fork de este repositorio.
* Resuelve el ejercicio.
* Comparte tu fork con @miguel_goi (Reporter access)

Si tienes alguna duda, puedes contactar con nosotros sin problema en miguel@letsgoi.com

Muchas gracias, ¡un saludo!



